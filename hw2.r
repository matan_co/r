train<-read.csv("train.csv",sep=",")
str(train)
test<-read.csv("test.csv",sep=",")
str(test)

#chacking NA
any(is.na(train))
any(is.na(test))
install.packages('caTools')
library('caTools')


#taking numeric columns
cols.numeric<-sapply(train,is.numeric)
cols.numeric

#train with only numeric values
train.numeric<-train[,cols.numeric]

#compute correlations
#Q1
cor.data<-cor(train.numeric)
install.packages("corrplot")
library("corrplot")
print(corrplot(cor.data,method='color'))

#compute linear module
#Q2
model<-lm(count ~ .,data=train)
print(summary(model))


#Q3
#data is not normalaized
res<- residuals(model)
hist(res, col = 'blue')

#making predictions on the test set
count.predicted <- predict(model,test)

#computing errors on the test set
results <- cbind(count.predicted, test$count)
colnames(results) <- c('Predicted', 'Actual')
results<- as.data.frame(results)

#cutting negative values

to_zero <- function(X){
    if(X<0){
        return (0);
    }
    else{
        return (x);
    }
}

#All negative results converts to zero
results$Predicted <- sapply(results$Predicted,to_zero)

#check
min(results$Predicted)

#Q4
#mean square errors
MSE <- mean((results$Actual-results$Predicted)^2)
RMSE <- MSE^0.5

#computing overfitting effect
count.predicted.train <- predict(model,train)
results.train <- cbind(count.predicted.train, train$count)
colnames(results.train)<- c('Predicted','Actual')
results<- as.data.frame(results.train)
results.train$Predicted <- sapply(results.train$Predicted,to_zero)

MSE.train <- mean((results.train$actual-(results.train$predicted)^2)
RMSE.train <- MSE.train^0.5
